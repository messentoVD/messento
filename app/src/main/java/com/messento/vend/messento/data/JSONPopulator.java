package com.messento.vend.messento.data;

/**
 * Created by VenD on 14/06/2017.
 */

import org.json.JSONObject;

public interface JSONPopulator {

    void Populate(JSONObject data);
}
