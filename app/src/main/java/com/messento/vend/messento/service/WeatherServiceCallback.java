package com.messento.vend.messento.service;

import com.messento.vend.messento.data.Channel;

/**
 * Created by VenD on 14/06/2017.
 */

public interface WeatherServiceCallback {
    void ServiceSuccess(Channel channel);

    void ServiceFailure(Exception exception);

}
