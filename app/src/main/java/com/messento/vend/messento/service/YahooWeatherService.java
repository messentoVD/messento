package com.messento.vend.messento.service;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.messento.vend.messento.data.Channel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by VenD on 14/06/2017.
 */

public class YahooWeatherService {

    private Exception error;
    private final String TAG = "MY MESSAGE";

    private WeatherServiceCallback callback;
    private String location;

    public YahooWeatherService(WeatherServiceCallback callback) {
        this.callback = callback;
    }

    public String getLocation() {
        return location;
    }

    public void RefreshWeather(final String location)
    {
        new AsyncTask<String, Void, String>()
        {
            @Override
            protected String doInBackground(String... strings) {

                Log.i(TAG, "WORKS JUSST FINE");

                String YQL = String.format("select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"%s\")", location);

                String endpoint = String.format("https://query.yahooapis.com/v1/public/yql?q=%s&format=json", Uri.encode(YQL));
                try {
                    URL url = new URL(endpoint);
                    URLConnection connection = url.openConnection();
                    InputStream inputStream = connection.getInputStream();
                    Log.i(TAG, "WORKS 2");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder result = new StringBuilder();
                    String line;
                    Log.i(TAG, "WORKS 3");
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                        Log.i(TAG, "WORKS 2");
                    }

                    return result.toString();

                }
                catch(IOException e)
                {
                    error = e;
                }


                return null;
            }

            @Override
            protected void onPostExecute(String s) {

                Log.i(TAG, "WORKS 76");
                if (s==null && error!=null)
                {
                    Log.i(TAG, " "+s);
                    callback.ServiceFailure(error);
                    return;
                }
                try
                {
                    Log.i(TAG, ""+s);
                    JSONObject data = new JSONObject(s);
                    JSONObject queryResults = data.optJSONObject("query");
                    int count = queryResults.optInt("count");

                    if (count == 0)
                    {
                        callback.ServiceFailure(new LocationWeatherException("No weather found"));
                        return;
                    }

                    Channel channel = new Channel();
                    Log.i(TAG, "WORKS 23");
                    channel.Populate(queryResults.optJSONObject("results").optJSONObject("channel"));
                    Log.i(TAG, "WORKS 25");
                    callback.ServiceSuccess(channel);


                }
                catch(JSONException e)
                {
                    callback.ServiceFailure(e);
                }
            }
        }.execute(location);
    }

    public class LocationWeatherException extends Exception{
        public LocationWeatherException(String detailMessage) {
            super(detailMessage);
        }
    }
}
