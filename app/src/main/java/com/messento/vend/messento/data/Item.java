package com.messento.vend.messento.data;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by VenD on 14/06/2017.
 */

public class Item implements JSONPopulator {

    final String TAG = "MY MESSAGE";
    private Condition condition;

    public Condition getCondition() {
        return condition;
    }

    @Override
    public void Populate(JSONObject data) {
        Log.i(TAG, "WORKS 2");
        condition = new Condition();
        condition.Populate(data.optJSONObject("condition"));
    }
}
