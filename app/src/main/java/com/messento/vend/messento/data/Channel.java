package com.messento.vend.messento.data;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by VenD on 14/06/2017.
 */

public class Channel implements JSONPopulator {

    private Units units;
    private Item item;

    public Units getUnits() {
        return units;
    }

    public Item getItem() {
        return item;
    }

    @Override
    public void Populate(JSONObject data) {

        units = new Units();
        units.Populate(data.optJSONObject("units"));

        item = new Item();
        item.Populate(data.optJSONObject("item"));



    }
}
