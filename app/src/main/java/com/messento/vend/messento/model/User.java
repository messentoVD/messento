package com.messento.vend.messento.model;

import java.util.Random;
import java.util.List;
import java.util.Queue;
import java.util.Random;

/**
 * Created by VenD on 13/06/2017.
 */

public class User {
    private String userID;
    private String displayName;
    private String email;
    private String photoURL;
    private String city;
    private Boolean status;

    public User() {
    }

    public User(String userID, String displayName, String email, String photoURL) {
        generateRandomCity();
        this.userID = userID;
        this.displayName = displayName;
        this.email = email;
        this.photoURL = photoURL;
        this.city = generateRandomCity();
        this.status = true;
    }

    private String generateRandomCity() {
        Random random = new Random();
        int number = random.nextInt(5);
        String cities[] = new String[] {"Karachi","Islamabad","Quetta","Peshawar","Lahore"};
        return cities[number];

    }

    public String getUserID() {
        return userID;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public String getCity() {
        return city;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
