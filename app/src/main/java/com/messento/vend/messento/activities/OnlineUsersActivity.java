package com.messento.vend.messento.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.messento.vend.messento.R;
import com.messento.vend.messento.RecycleViewAdapter;
import com.messento.vend.messento.model.User;

import java.util.ArrayList;
import java.util.List;

public class OnlineUsersActivity extends AppCompatActivity {

    RecyclerView mylist;
    private RecycleViewAdapter adapter;
    private DatabaseReference databaseUsers;
    List<User> ActiveUsers = new ArrayList<>();
    final String TAG = "MyMessage";
    String primaryuser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_users);
        mylist = (RecyclerView) findViewById(R.id.recyclerView);
        primaryuser = getIntent().getStringExtra("UserID");
        adapter = new RecycleViewAdapter(getBaseContext(), ActiveUsers, primaryuser);
        mylist.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        mylist.setAdapter(adapter);
        databaseUsers = FirebaseDatabase.getInstance().getReference("users");
        databaseUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ActiveUsers.clear();
                for (DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                    User user = usersnapshot.getValue(User.class);
                    if (user.getStatus()&& !(user.getUserID().equals(primaryuser))) {
                        //Log.i(TAG,user.getDisplayName()+"    primary   "+primaryuser+"    ID   "+user.getUserID());
                        ActiveUsers.add(user);
                    }
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    protected void onUserLeaveHint() {
        super.onUserLeaveHint();

    }

    private void exitAppAnimate() {
        databaseUsers = FirebaseDatabase.getInstance().getReference().child("users").child(primaryuser).child("status");
        databaseUsers.setValue(false);
        finish();
        //System.exit(0);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        exitAppAnimate();
    }
}
