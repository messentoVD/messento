package com.messento.vend.messento.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.messento.vend.messento.ChatMessage;
import com.messento.vend.messento.R;

import java.util.ArrayList;


public class MessageScreen extends AppCompatActivity {

    EditText input_text;
    ImageButton submit_btn;
    public ListView listViewOfMessages;
    DatabaseReference databaseChat;
    ArrayList<String> messageList = new ArrayList<String>();
    Intent intent;
    String primaryUser;
    String secondaryUser;
    String primaryUserName;


    String querryDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_screen);
        
        intent = getIntent();
        input_text = (EditText) findViewById(R.id.input);
        submit_btn = (ImageButton) findViewById(R.id.btn_input);
        listViewOfMessages = (ListView) findViewById(R.id.listView);

        //getting variables from previous screen
        primaryUser = intent.getStringExtra("primaryuser");
        secondaryUser = intent.getStringExtra("secondaryuser");

        //Get the name of primary User
        primaryUserName = getPrimaryUserName(primaryUser);

        //Making appropriate Key to querry database
        querryDatabase = makeKeytoQuerry();

        //Fetch all messages from database
        fetchAllMessages();

        //Enabling Enter Button to send message
        enableEnterToSendMessage();

        //Send Button
        submit_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addMessageToDatabase();
            }
        });
    }


    public void enableEnterToSendMessage()
    {
        input_text.setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    switch (keyCode)
                    {
                        case KeyEvent.KEYCODE_ENTER:
                            addMessageToDatabase();
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

    }
    public String  makeKeytoQuerry()
    {
        return (primaryUser.compareTo(secondaryUser) < 0)? (primaryUser + "_" + secondaryUser) : (querryDatabase = secondaryUser + "_" + primaryUser);
    }

    //Connect to Database and fetch all messages
    public void fetchAllMessages()
    {
        databaseChat = FirebaseDatabase.getInstance().getReference("Message/" + querryDatabase);

        databaseChat.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listViewOfMessages.setAdapter(null);

                messageList.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    String messageId = ds.getKey().toString();
                    String split[] = messageId.split("_");
                    String senderName = split[2];

                    for (DataSnapshot d : ds.getChildren()) {
                        if(d.getKey().toString().equals("text"))
                        {
                            messageList.add(senderName + ":" + d.getValue().toString());

                        }

                    }

                }
                for(int i=0 ; i<messageList.size() ; i++)
                    Log.e("" + i +" Element",messageList.get(i));
                ArrayAdapter arrayAdapter = new ArrayAdapter(MessageScreen.this, android.R.layout.simple_list_item_1,messageList);
                listViewOfMessages.setAdapter(arrayAdapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public String getPrimaryUserName(String primaryUserID) {
        databaseChat = FirebaseDatabase.getInstance().getReference("users/" + primaryUserID);
        databaseChat.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    if (ds.getKey().toString().equals("displayName")) {

                        primaryUserName = ds.getValue().toString();

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
        return primaryUserName;
    }



    //Update Send Message to database
    public void addMessageToDatabase()
    {
        String text = input_text.getText().toString();
        if(!text.equals(""))
        {
            ChatMessage msg = new ChatMessage(text,primaryUser,secondaryUser);
            databaseChat = FirebaseDatabase.getInstance().getReference("Message/" + querryDatabase);
            databaseChat.child("message_" + msg.getTime() + "_" + primaryUserName).setValue(msg);
            input_text.setText("");
        }
    }
}
