package com.messento.vend.messento;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.messento.vend.messento.activity.MessageScreen;
import com.messento.vend.messento.model.User;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

/**
 * Created by VenD on 14/06/2017.
 */

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.RecycleViewHolder> {

    private LayoutInflater inflator;
    List<User> userlist = Collections.emptyList();
    final String TAG = "MyMessage";
    Context context;
    String primaryuser;

    public RecycleViewAdapter(Context context, List<User> userlist,String puser) {

        inflator = LayoutInflater.from(context);
        this.userlist = userlist;
        this.context = context;
         this.primaryuser= puser;
    }

    @Override
    public RecycleViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View myView = inflator.inflate(R.layout.row_layout, null);
        RecycleViewHolder holder = new RecycleViewHolder(myView, context, userlist,primaryuser);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecycleViewHolder holder, int i) {
        User current = userlist.get(i);
        holder.text.setText(current.getDisplayName() == null ? "Hello World" : current.getDisplayName());
        Picasso.with(context).load(current.getPhotoURL()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return userlist.size();
    }

    class RecycleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        TextView text;
        ImageView image;
        Context Viewcontext;
        List<User> onlineusers = Collections.emptyList();


        public RecycleViewHolder(View itemView, Context context, List<User> team, String primaryuser) {
            super(itemView);
            this.Viewcontext = context;
            this.onlineusers = team;
            itemView.setOnClickListener(this);
            itemView.setLongClickable(true);
            itemView.setOnLongClickListener((View.OnLongClickListener) this);
            text = (TextView) itemView.findViewById(R.id.listText);
            image = (ImageView) itemView.findViewById(R.id.listImage);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            Log.i(TAG, "" + position);
            User userclicked = this.onlineusers.get(position);
            Intent chatIntent = new Intent(itemView.getContext(), MessageScreen.class);
            chatIntent.putExtra("secondaryuser", userclicked.getUserID());
            chatIntent.putExtra("primaryuser", primaryuser);
            itemView.getContext().startActivity(chatIntent);

        }


        @Override
        public boolean onLongClick(View view) {

            int position = getAdapterPosition();
            Log.i(TAG, "" + position);
            User userclicked = this.onlineusers.get(position);
//          Toast.makeText(cnt, userclicked.getDisplayName(), Toast.LENGTH_SHORT).show();
            Intent chatIntent = new Intent(itemView.getContext(), UserProfileActivity.class);
            chatIntent.putExtra("secondaryuser", userclicked.getUserID());
            itemView.getContext().startActivity(chatIntent);

            return true;
        }

    }
}