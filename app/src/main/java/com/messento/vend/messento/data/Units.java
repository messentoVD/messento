package com.messento.vend.messento.data;

import org.json.JSONObject;

/**
 * Created by VenD on 14/06/2017.
 */

public class Units implements JSONPopulator {

    private String temperature;
    @Override
    public void Populate(JSONObject data) {

        temperature = data.optString("temperature");
    }

    public String getTemperature() {
        return temperature;
    }
}
