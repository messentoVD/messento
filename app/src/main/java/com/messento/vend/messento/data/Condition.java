package com.messento.vend.messento.data;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by VenD on 14/06/2017.
 */

public class Condition implements JSONPopulator {
    private int code;
    private int temperature;
    private String description;
    final String TAG = "MY MESSAGE";

    public int getCode() {
        return code;
    }

    public int getTemperature() {
        return temperature;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public void Populate(JSONObject data) {
        Log.e(TAG,data.toString());
        Log.i(TAG, "WORKS 25790");
        code = data.optInt("code");
        temperature = data.optInt("temp");
        description = data.optString("text");

    }
}
