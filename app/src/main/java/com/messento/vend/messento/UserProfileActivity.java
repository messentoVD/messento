package com.messento.vend.messento;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.messento.vend.messento.data.Channel;
import com.messento.vend.messento.data.Item;
import com.messento.vend.messento.model.User;
import com.messento.vend.messento.service.WeatherServiceCallback;
import com.messento.vend.messento.service.YahooWeatherService;
import com.squareup.picasso.Picasso;

public class UserProfileActivity extends AppCompatActivity implements WeatherServiceCallback {

    private ImageView WeatherImage;
    private TextView TemperatureTextView;
    private TextView ConditionTextView;
    private TextView LocationTextView;
    private ImageView UserImage;
    private TextView UserName;
    private String location="Karachi, PK";

    private DatabaseReference databaseChat;
    private YahooWeatherService service;
    private ProgressDialog dialog;
    private String temporaryLocation = "PK";
    private final String TAG="mesage";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        View backgroundimage = findViewById(R.id.ConstraintLayout);
        Drawable background = backgroundimage.getBackground();
        background.setAlpha(90);
        //intent wala scene
        Intent i = getIntent();



        UserImage = (ImageView)findViewById(R.id.UserImage);
        UserName = (TextView)findViewById(R.id.UserName);
        WeatherImage = (ImageView) findViewById(R.id.WeatherImage);
        TemperatureTextView = (TextView)findViewById(R.id.TemperatureTextView);
        ConditionTextView = (TextView)findViewById(R.id.ConditionTextView);
        LocationTextView = (TextView)findViewById(R.id.LocationTextView);

        User user = new User();


        String userID = i.getStringExtra("secondaryuser");

        //userID = i.getStringExtra(User.userID);


        databaseChat = FirebaseDatabase.getInstance().getReference("users/"+userID);


        databaseChat.addValueEventListener(new ValueEventListener() {
                                               @Override
                                               public void onDataChange(DataSnapshot dataSnapshot) {
                                                   for (DataSnapshot ds : dataSnapshot.getChildren()) {

                                                       if(ds.getKey().toString().equals("displayName")) {

                                                           String userName = ds.getValue().toString();
                                                           UserName.setText(userName);
                                                       }
                                                       if(ds.getKey().toString().equals("photoURL")) {

                                                           Log.e(TAG, "text: "+ds.getValue().toString());
                                                           //int resourceId = getResources().getIdentifier(ds.getValue().toString(), null, getPackageName());
                                                           //Drawable UserIconDrawable = getResources().getDrawable(resourceId);
                                                           //UserImage.setImageDrawable(UserIconDrawable);
                                                           String url = ds.getValue().toString();


                                                           Picasso.with(getApplicationContext()).load(url).into(UserImage);

                                                       }

                                                       if (ds.getKey().toString().equals("city") && ds.getValue().toString()!=null)
                                                       {
                                                            location = ds.getValue().toString();
                                                            location+="PK";
                                                       }
                                                   }
                                               }

                                               @Override
                                               public void onCancelled(DatabaseError databaseError) {

                                               }
                                           });

        service = new YahooWeatherService(this);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.show();

        //set the location here
        LocationTextView.setText(location);
        service.RefreshWeather(location);
    }

    @Override
    public void ServiceSuccess(Channel channel) {
        dialog.hide();
        Item item = channel.getItem();
        int resourceId = getResources().getIdentifier("drawable/icon_"+channel.getItem().getCondition().getCode(), null, getPackageName());
        Drawable weatherIconDrawable = getResources().getDrawable(resourceId);
        WeatherImage.setImageDrawable(weatherIconDrawable);
        //LocationTextView.setText(service.getLocation());
        TemperatureTextView.setText(item.getCondition().getTemperature() + " " + channel.getUnits().getTemperature());
        ConditionTextView.setText(item.getCondition().getDescription());
    }

    @Override
    public void ServiceFailure(Exception exception) {
        dialog.hide();
        Toast.makeText(this, exception.getMessage(), Toast.LENGTH_LONG).show();
    }
}