package com.messento.vend.messento;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by VenD on 6/14/2017.
 */

public class ChatMessage {
    private String primaryUser;
    private String secondaryUser;
    private String time;
    private String text;

    public ChatMessage(String text, String primaryUser,String secondaryUser) {
        this.primaryUser = primaryUser;
        this.secondaryUser = secondaryUser;
        this.text = text;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSS");
        this.time = sdf.format(new Date());
}

    public ChatMessage(){

    }

    public String getText() {
        return text;
    }


    public String getPrimaryUser() {
        return primaryUser;
    }

    public String getSecondaryUser() {
        return secondaryUser;
    }

    public String getTime() {
        return time;
    }


}
